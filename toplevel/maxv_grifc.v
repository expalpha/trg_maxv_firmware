// CPLD flash<->fpga bridge
//  -------------------------------------------------------------------
//     CFI flash             cpld              Quad Spi flash has ...
//   ____________      __________________         flash sck[]   
//  | Addr[24:0] |    | Flash_addr[24:0] |        flash ncs[] 
//  | NCE        |    | Flash_nce        |        flash io0-3[] 
//  | NWE        |    | Flash_nwe        |             
//  | NOE        |    | Flash_noe        |     Nand flash has ... ?
//  | DATA[16:0] |    | Flash_data       |
//  |____________|    | Flash_nce        |                Fpga
//                    |                  |             ___________ 
//                    |   fpga_conf_done |   pullup   | CONF_DONE |  
//                    |   fpga_nstatus   |   pullup   | nSTATUS   |  
//                    |   fpga_nconfig   |   pullup   | nCONFIG   |  
//                    |   fpga_data 1or8 |            | DATA      |  
//                    |   fpga_dclk      |            | DCLK      |  
//                    |__________________|     Gnd    |_nCE_______|  
//
// **tri-state all unused I/O pins [default is to ground them]**
// ports pfl_flash_access_request/granted indicate when pfl using flash
//   [flash pins *must* be tristated during non-max flash access]
//
//  can make separate "flash program only" max config to use when
//    updating flash, then can use smaller fpga-config only max config
//  -------------------------------------------------------------------
//  FPL handles up to 8 pages per flash block (flashsize -> size/num pages)
//     each page holds config data for one FPGA-chain [ >= 1 sof]
//        generate pof from all the sofs.  page-addresses: Block/Start/Auto
//           Block - specify start and end [starts on 8kb boundary]
//           Start - speicfy start only [must be on 8kb boundary]
//           Auto  - quartus decides (starts are on 128kb boundary)
// 
//  need 256-byte space for option area [page addresses, versions etc.]
//     The address of this block is stored in the pof (where?)
// 
//  Remote Upgrade: store several fpga configs, and trigger fpga reconfig
//     from a different page ...    [all sync to pfl_clk ...]
//        FPGA_PGM[2:0] selects page
//        once set, release pft_nreset
//        wait 4-5 clks, pulse pfl_nreconfigure low >= 1 clk
//           pfl_watchdog_error high => fpga isn't resetting timer
//          **Fpga must have watchdog timer reset, if this is used**
//  pfl_reset_watchdog is input from fpga to PFL hold high >= 2 pfl clks
//  -------------------------------------------------------------------
//  Flash is pc28f512m29ew [64Mbytes]
//  -------------------------------------------------------------------
module maxv_grifc( // directions as on toplevel of grifc schematic
   inout  wire CONF_DONE,           // pulled low during config, high=> success
   output wire NCONFIG,             // pulse low to reset fpga and start config
   inout  wire NSTATUS,             // from fpga, stays high unless error
   output wire DCLK,                output wire [7:0] CFG_D,   // also in bank_1C
   // stratix i/o in bank_1C [clk is in clock/config, from oscillator]
   input  wire CLK_50M,             input  wire CFG_REQUEST,   //  -   U31
   input  wire CFG_RESET,           input  wire CFG_SCLK,      // V28, V31
   input  wire CFG_DIN,             output wire CFG_DOUT,      // W33, V30
   inout  wire [2:0] CFG_SEL,       input  wire VME_SYSRESETn, // 2:W32,1:W28,0:V29
   
   output wire FLASH_WPn,           output wire FLASH_nWE,
   output wire FLASH_BYTEn,         output wire FLASH_RSTn,
   output wire FLASH_nOE,           output wire [1:0] FLASH_nCE,
   output wire [24:0] FLASH_A,      inout wire [15:0] FLASH_D,
   input  wire FLASH_BUSYn          // low during program/erase
);
assign   CFG_SEL     = {3{1'bz}};
assign	FLASH_WPn   = flash_access ? 1'b1 : 1'bz;
assign	FLASH_BYTEn = flash_access ? 1'b1 : 1'bz;
assign	FLASH_RSTn  = flash_access ? 1'b1 : 1'bz;

reg CLK_25;          always @ (posedge CLK_50M) CLK_25 <= ~CLK_25;
//reg [1:0] req_sync;  always @ (posedge CLK_50M) req_sync <= {req_sync[0],CFG_REQUEST};

   reg req1, req2, req3;
   reg reqout1, reqout2, reqout3;
   reg req;
   always @ (posedge CLK_50M) begin
      req1 <= CFG_REQUEST;
      req2 <= req1;
      req3 <= req2;
      reqout1  <= 0;
      if (req3 == 0 && req2 == 1) begin
         reqout1 <= 1;
      end
      reqout2 <= reqout1;
      reqout3 <= reqout2;
      req <= reqout1 | reqout2 | reqout3;
   end

   assign CFG_DOUT = req;

wire flash_access; // pfl_flash_access_request below is output (asking for access)
         // **driving pfl_flash_access_grant   low => NO Jtag access to flash/fpga_config
par_flash_load	pfl (
   .pfl_clk(CLK_50M),                        .pfl_nreset(1'b1),
   .pfl_flash_access_request(flash_access),  .pfl_flash_access_granted(flash_access),

   .fpga_pgm(3'h0),
   //.pfl_nreconfigure(1'b1 /*req_sync[1]*/),
   //.pfl_nreconfigure(~req_sync[1]),
   .pfl_nreconfigure(~req),

   .fpga_conf_done(CONF_DONE),
   .fpga_nconfig(NCONFIG),                   .fpga_nstatus(NSTATUS),
   .fpga_dclk(DCLK),                         .fpga_data(CFG_D),
   /*.pfl_watchdog_error(fpga_watchdog),*/   .pfl_reset_watchdog(CLK_25), // 2 pfl clks

   .flash_nwe(FLASH_nWE),                    .flash_noe(FLASH_nOE),
   .flash_nce(FLASH_nCE),
   .flash_addr(FLASH_A),                     .flash_data(FLASH_D)
);

endmodule
