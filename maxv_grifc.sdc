# Clock constraints
create_clock -name "clk50" -period 20.000ns [get_ports {CLK_50M}] -waveform {0.000 10.000}

# Automatically constrain PLL and other generated clocks
derive_pll_clocks -create_base_clocks

# Automatically calculate clock uncertainty to jitter and other effects.
derive_clock_uncertainty

# tsu/th constraints

# tco constraints

# tpd constraints

# ignore timing problems with signaltap
# set_multicycle_path -to {sld_signaltap:*} -setup -end 2
# set_multicycle_path -to {sld_signaltap:*} -hold  -end 2
#set_false_path -to {sld_signaltap:*}

# ignore timing failure on clk transfer synchronisers
# (they are there because they will fail timing)
#set_false_path -to {sync*}
